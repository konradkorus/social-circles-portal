﻿namespace SocialCirclesPortal.Infrastructure.Web.Mapping
{
    public interface IMapperResolver
    {
        TDestination Map<TDestination>(object source);
    }
}