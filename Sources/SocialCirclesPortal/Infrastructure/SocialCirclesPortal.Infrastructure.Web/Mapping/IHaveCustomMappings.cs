﻿using AutoMapper;

namespace SocialCirclesPortal.Infrastructure.Web.Mapping
{
    public interface IHaveCustomMappings
    {
        void CreateMappings(IConfiguration configuration);
    }
}