using AutoMapper;

namespace SocialCirclesPortal.Infrastructure.Web.Mapping
{
    public class MapperResolver : IMapperResolver
    {
        public TDestination Map<TDestination>(object source)
        {
            return Mapper.Map<TDestination>(source);
        }
    }
}