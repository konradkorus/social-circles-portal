﻿namespace SocialCirclesPortal.Infrastructure.Web.ConfigurationProvider
{
    public interface IConfigurationProvider
    {
        /// <summary>
        /// Path to algorithm executable file.
        /// </summary>
        string AlgorithmFilePath { get; }

        /// <summary>
        /// Path to algorithm databases (facebook/twitter/google).
        /// </summary>
        string AlgorithmDatabasesPath { get; }

        /// <summary>
        /// Path to algorithm results.
        /// </summary>
        string AlgorithmResultsPath { get; }
    }
}