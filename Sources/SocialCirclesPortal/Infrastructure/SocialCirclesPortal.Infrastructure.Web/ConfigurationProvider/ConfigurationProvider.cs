﻿using System.Configuration;
using System.IO;
using SocialCirclesPortal.Infrastructure.Web.ApplicationPathResolver;

namespace SocialCirclesPortal.Infrastructure.Web.ConfigurationProvider
{
    public class ConfigurationProvider : IConfigurationProvider
    {
        private readonly string _serverPath;

        public ConfigurationProvider(IApplicationPathResolver applicationPathResolver)
        {
            _serverPath = applicationPathResolver.GetAbsolutePath("~/");
        }

        /// <summary>
        /// Path to algorithm executable file.
        /// </summary>
        public string AlgorithmFilePath
        {
            get
            {
                return Path.Combine(_serverPath, ConfigurationManager.AppSettings["AlgorithmFilePath"]);
            }
        }

        /// <summary>
        /// Path to algorithm databases (facebook/twitter/google).
        /// </summary>
        public string AlgorithmDatabasesPath
        {
            get
            {
                return Path.Combine(_serverPath, ConfigurationManager.AppSettings["AlgorithmDatabasesPath"]);
            }
        }

        /// <summary>
        /// Path to algorithm results.
        /// </summary>
        public string AlgorithmResultsPath
        {
            get
            {
                return Path.Combine(_serverPath, ConfigurationManager.AppSettings["AlgorithmResultsPath"]);
            }
        }
    }
}