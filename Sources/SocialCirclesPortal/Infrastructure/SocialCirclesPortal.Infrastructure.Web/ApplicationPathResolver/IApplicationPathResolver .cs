﻿namespace SocialCirclesPortal.Infrastructure.Web.ApplicationPathResolver
{
    public interface IApplicationPathResolver 
    {
        string GetAbsolutePath(string virtualPath); 
    }
}