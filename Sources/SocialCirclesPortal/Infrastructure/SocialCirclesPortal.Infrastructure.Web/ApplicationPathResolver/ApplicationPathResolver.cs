﻿using System.Web.Hosting;

namespace SocialCirclesPortal.Infrastructure.Web.ApplicationPathResolver
{
    public class ApplicationPathResolver : IApplicationPathResolver
    {
        public string GetAbsolutePath(string virtualPath)
        {
            return HostingEnvironment.MapPath(virtualPath);
        }
    }
}