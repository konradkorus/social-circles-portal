﻿using System;
using System.Diagnostics;
using System.IO;

namespace SocialCirclesPortal.Infrastructure.Web.ProcessRunner
{
    public class ProcessRunner : IProcessRunner
    {
        public event EventHandler<ProcessRunnerEventArgs> ProcessExecuted;

        public void Run(string fileName, string arguments)
        {
            try
            {
                var processStartInfo = new ProcessStartInfo(fileName)
                {
                    UseShellExecute = false,
                    Arguments = arguments,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                };

                using (var process = new Process())
                {
                    process.StartInfo = processStartInfo;
                    process.Start();

                    StreamReader sr = process.StandardOutput;
                    string standardOutput = sr.ReadToEnd();
                    process.WaitForExit();

                    if (ProcessExecuted != null)
                    {
                        ProcessExecuted(this, new ProcessRunnerEventArgs(standardOutput));
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}