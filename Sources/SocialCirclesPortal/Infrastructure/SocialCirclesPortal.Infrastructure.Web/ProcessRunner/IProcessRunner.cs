﻿using System;

namespace SocialCirclesPortal.Infrastructure.Web.ProcessRunner
{
    public interface IProcessRunner
    {
        event EventHandler<ProcessRunnerEventArgs> ProcessExecuted;

        void Run(string fileName, string arguments);
    }
}