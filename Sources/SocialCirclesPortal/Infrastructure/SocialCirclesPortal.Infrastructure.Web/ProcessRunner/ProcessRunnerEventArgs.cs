using System;

namespace SocialCirclesPortal.Infrastructure.Web.ProcessRunner
{
    public class ProcessRunnerEventArgs : EventArgs
    {
        private readonly string _standardOutput;

        public ProcessRunnerEventArgs(string standardOutput)
        {
            _standardOutput = standardOutput;
        }

        public string StandardOutput
        {
            get { return _standardOutput; }
        }
    }
}