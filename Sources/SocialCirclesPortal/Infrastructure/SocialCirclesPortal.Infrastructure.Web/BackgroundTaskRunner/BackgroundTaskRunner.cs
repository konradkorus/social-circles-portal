﻿using System;
using System.Threading;
using System.Web.Hosting;

namespace SocialCirclesPortal.Infrastructure.Web.BackgroundTaskRunner
{
    public class BackgroundTaskRunner : IBackgroundTaskRunner
    {
        public void QueueTask(Action<CancellationToken> task)
        {
            HostingEnvironment.QueueBackgroundWorkItem(task);
        }
    }
}