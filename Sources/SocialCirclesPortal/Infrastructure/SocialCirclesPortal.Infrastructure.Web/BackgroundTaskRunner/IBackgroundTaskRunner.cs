﻿using System;
using System.Threading;

namespace SocialCirclesPortal.Infrastructure.Web.BackgroundTaskRunner
{
    public interface IBackgroundTaskRunner
    {
        void QueueTask(Action<CancellationToken> task);
    }
}