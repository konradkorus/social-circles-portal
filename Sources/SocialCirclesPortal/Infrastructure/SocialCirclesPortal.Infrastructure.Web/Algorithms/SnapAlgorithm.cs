﻿using SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner;
using SocialCirclesPortal.Infrastructure.Web.ConfigurationProvider;

namespace SocialCirclesPortal.Infrastructure.Web.Algorithms
{
    /// <summary>
    /// SNAP algorithm.
    /// </summary>
    public class SnapAlgorithm : IAlgorithm
    {
        private readonly IConfigurationProvider _configurationProvider;

        public IAlgorithmArguments Arguments { get; set; }

        public SnapAlgorithm(IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public string AlgorithmFilePath
        {
            get { return _configurationProvider.AlgorithmFilePath; }
        }

        public void ProcessAlgorithmResult(string standardOutput)
        {

        }
    }
}