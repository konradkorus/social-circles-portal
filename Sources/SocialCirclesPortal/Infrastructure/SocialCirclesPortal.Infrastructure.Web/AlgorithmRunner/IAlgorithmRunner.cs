﻿namespace SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner
{
    public interface IAlgorithmRunner
    {
        void RunAlgorithm(IAlgorithm algorithm);
    }
}