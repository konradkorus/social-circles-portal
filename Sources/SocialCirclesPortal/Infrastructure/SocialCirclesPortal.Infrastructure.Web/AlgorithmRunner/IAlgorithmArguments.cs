﻿namespace SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner
{
    public interface IAlgorithmArguments
    {
        string ArgumentsToString();
    }
}