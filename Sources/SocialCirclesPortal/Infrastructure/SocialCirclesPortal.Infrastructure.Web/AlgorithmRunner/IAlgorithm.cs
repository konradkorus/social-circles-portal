namespace SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner
{
    public interface IAlgorithm
    {
        string AlgorithmFilePath { get; }

        IAlgorithmArguments Arguments { get; set; }

        void ProcessAlgorithmResult(string standardOutput);
    }
}