﻿using SocialCirclesPortal.Infrastructure.Web.BackgroundTaskRunner;
using SocialCirclesPortal.Infrastructure.Web.ProcessRunner;

namespace SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner
{
    /// <summary>
    /// Runs algorithm in background.
    /// </summary>
    public class AlgorithmRunner : IAlgorithmRunner
    {
        private readonly IBackgroundTaskRunner _backgroundTaskRunner;
        private readonly IProcessRunner _processRunner;

        public AlgorithmRunner(IBackgroundTaskRunner backgroundTaskRunner, IProcessRunner processRunner)
        {
            _backgroundTaskRunner = backgroundTaskRunner;
            _processRunner = processRunner;
        }

        public void RunAlgorithm(IAlgorithm algorithm)
        {
            _processRunner.ProcessExecuted += (sender, eventArgs) => algorithm.ProcessAlgorithmResult(eventArgs.StandardOutput);
            _backgroundTaskRunner.QueueTask(token => _processRunner.Run(algorithm.AlgorithmFilePath, algorithm.Arguments.ArgumentsToString()));
        }
    }
}