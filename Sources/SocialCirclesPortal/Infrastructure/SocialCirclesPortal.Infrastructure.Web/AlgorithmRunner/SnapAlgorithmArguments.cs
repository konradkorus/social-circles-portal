﻿namespace SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner
{
    public class SnapAlgorithmArguments : IAlgorithmArguments
    {
        public string Ego { get; set; }

        public string K { get; set; }

        public string Lambda { get; set; }

        public string Training { get; set; }

        public string Gradient { get; set; }

        public string Improve { get; set; }

        public string Restarts { get; set; }

        public string ResultFilePath { get; set; }

        public string ArgumentsToString()
        {
            return string.Format("-i {0} -k {1} -l {2} -t {3} -g {4} -q {5} -r {6}\\414.txt", Ego, K, Lambda, Training,
                Gradient, Improve, ResultFilePath);
        }
    }
}