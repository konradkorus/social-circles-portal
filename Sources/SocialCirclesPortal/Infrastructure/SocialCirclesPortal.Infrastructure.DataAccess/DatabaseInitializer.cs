using System.Data.Entity;
using EfEnumToLookup.LookupGenerator;

namespace SocialCirclesPortal.Infrastructure.DataAccess
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    //public class DatabaseInitializer : DropCreateDatabaseAlways<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            var enumToLookup = new EnumToLookup { TableNamePrefix = string.Empty };
            enumToLookup.Apply(context);

            base.Seed(context);
        }
    }
}