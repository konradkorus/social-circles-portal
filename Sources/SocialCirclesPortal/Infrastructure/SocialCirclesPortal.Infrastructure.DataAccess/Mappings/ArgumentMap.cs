﻿using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Infrastructure.DataAccess.Mappings
{
    public class ArgumentMap : BaseMap<Argument, int>
    {
        public ArgumentMap()
        {
            ToTable("Argument");

            Property(x => x.Min).IsRequired();
            Property(x => x.Max).IsRequired();
            Property(x => x.Step).IsRequired();
            Property(x => x.From).IsRequired();
            Property(x => x.To).IsRequired();
            Property(x => x.ArgumentType).IsRequired();
            //Property(x => x.SeriesId);

            //HasOptional(x => x.Series).WithMany(x => x.Arguments).WillCascadeOnDelete(true);
            HasRequired(x => x.Series).WithMany(x => x.Arguments);
            HasMany(a => a.Values).WithRequired(a => a.Argument);
                //.HasForeignKey(x => x.SeriesId).WillCascadeOnDelete(true);

            //HasMany(x => x.Values);
        }
    }
}