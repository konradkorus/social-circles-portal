﻿using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Infrastructure.DataAccess.Mappings
{
    public class SeriesMap : BaseMap<Series, int>
    {
        public SeriesMap()
        {
            ToTable("Series");

            Property(x => x.Name).IsRequired();
            Property(x => x.CreateDateTime).IsRequired();
            Property(x => x.ChangeDateTime).IsOptional();

            //HasMany(x => x.Arguments);
            HasMany(a => a.Arguments).WithRequired(a => a.Series);//.HasForeignKey(a => a.SeriesId);
            //HasMany(a => a.Arguments).WithRequired(a => a.Series);

            //HasRequired(x => x.KArgument).WithMany().WillCascadeOnDelete(false);
            //HasRequired(x => x.LambArgument).WithMany().WillCascadeOnDelete(false);

            //HasMany(x => x.Arguments);
            //HasRequired(x => x.Iterations).WithMany().HasForeignKey(x => x.IterationsId).WillCascadeOnDelete(true);
            //HasRequired(x => x.K).WithMany().WillCascadeOnDelete(false);
            //HasRequired(x => x.Lambda).WithMany().WillCascadeOnDelete(false);
            //HasRequired(x => x.Gradient).WithMany().HasForeignKey(x => x.GradientId).WillCascadeOnDelete(true);
            //HasRequired(x => x.Improve).WithMany().HasForeignKey(x => x.ImproveId).WillCascadeOnDelete(true);
            //HasRequired(x => x.Training).WithMany().HasForeignKey(x => x.TrainingId).WillCascadeOnDelete(true);

            //HasRequired(x => x.Iterations).WithMany().HasForeignKey(x => x.IterationsId);
            //HasRequired(x => x.K).WithMany().HasForeignKey(x => x.KId);
            //HasRequired(x => x.Lambda).WithMany().HasForeignKey(x => x.LambdaId);
            //HasRequired(x => x.Gradient).WithMany().HasForeignKey(x => x.GradientId);
            //HasRequired(x => x.Improve).WithMany().HasForeignKey(x => x.ImproveId);
            //HasRequired(x => x.Training).WithMany().HasForeignKey(x => x.TrainingId);
        }
    }
}