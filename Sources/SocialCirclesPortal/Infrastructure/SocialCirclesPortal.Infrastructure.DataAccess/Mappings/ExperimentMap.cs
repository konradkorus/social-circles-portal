﻿using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Infrastructure.DataAccess.Mappings
{
    public class ExperimentMap : BaseMap<Experiment, int>
    {
        public ExperimentMap()
        {
            ToTable("Experiment");

            Property(x => x.Result1).IsRequired();
            Property(x => x.Result1).IsRequired();
        }
    }
}