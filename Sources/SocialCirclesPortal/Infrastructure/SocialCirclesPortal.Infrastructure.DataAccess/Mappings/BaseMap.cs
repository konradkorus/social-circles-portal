﻿using System.Data.Entity.ModelConfiguration;
using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Infrastructure.DataAccess.Mappings
{
    public class BaseMap<TEntity, TEntityKey> : EntityTypeConfiguration<TEntity> where TEntity : Entity<TEntityKey>
    {
        public BaseMap()
        {
            // Primary Key
            HasKey(x => x.Id);
        }
    }
}