﻿using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Infrastructure.DataAccess.Mappings
{
    public class ArgumentValueMap : BaseMap<ArgumentValue, int>
    {
        public ArgumentValueMap()
        {
            ToTable("ArgumentValue");

            Property(x => x.Value).IsRequired();
            HasRequired(x => x.Argument).WithMany(x => x.Values);
        }
    }
}