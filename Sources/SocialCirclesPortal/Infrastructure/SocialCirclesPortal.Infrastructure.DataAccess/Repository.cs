using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using SocialCirclesPortal.Domain.Repository;

namespace SocialCirclesPortal.Infrastructure.DataAccess
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IDataContext _context;
        private readonly IDbSet<TEntity> _dbSet;

        public Repository(IDataContext context)
        {
            _context = context;
            _dbSet = _context.GetEntitySet<TEntity>();
        }

        public IQueryable<TEntity> Queryable()
        {
            return _dbSet;
        }

        public IDataContext Context
        {
            get { return _context; }
        }

        private IDbSet<TEntity> DbSet
        {
            get { return _dbSet; }
        }

        public virtual TEntity Find(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        public virtual void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public virtual void Update(TEntity entity)
        {
            _context.ChangeState(entity, EntityState.Modified);
        }

        public virtual void Delete(object id)
        {
            var entity = _dbSet.Find(id);
            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.AsQueryable();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> wherePredicate,
            params Expression<Func<TEntity, object>>[] includes)
        {
            //return _dbSet.AsQueryable().Aggregate(_dbSet, (current, expression) => current.Include(expression)).Where(wherePredicate).ToList();
            ////IQueryable<TEntity> query = GetRefreshedObjectSet();
            ////query = includes.Aggregate(query, (current, expression) => current.Include(expression));
            ////return query.Where(wherePredicate).ToList();
            return null;
        }

        public IQueryable<TEntity> Select(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            int? page = null,
            int? pageSize = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (filter != null)
            {
                query = query.AsExpandable().Where(filter);
            }
            if (page != null && pageSize != null)
            {
                query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }
            return query;
        }
    }
}