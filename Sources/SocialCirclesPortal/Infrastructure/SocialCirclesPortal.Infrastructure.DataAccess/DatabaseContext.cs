using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using SocialCirclesPortal.Domain.Repository;
using SocialCirclesPortal.Infrastructure.DataAccess.Mappings;

namespace SocialCirclesPortal.Infrastructure.DataAccess
{
    public class DatabaseContext : DbContext, IDataContext
    {
        private bool _disposed;

        public DatabaseContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            Database.SetInitializer(new DatabaseInitializer());

            this.Configuration.ValidateOnSaveEnabled = true;
        }

        public DatabaseContext() : this("Name=SocialCirclesPortalContext")
        {
        }

        public IDbSet<TEntity> GetEntitySet<TEntity>() where TEntity : class
        {
            return this.Set<TEntity>();
        }

        public DbQuery<TEntity> GetDbQuery<TEntity>() where TEntity : class
        {
            return this.Set<TEntity>().AsNoTracking();
        }

        public void ChangeState<TEntity>(TEntity entity, EntityState entityState) where TEntity : class
        {
            this.Entry(entity).State = entityState;
        }

        public override int SaveChanges()
        {
            //return base.SaveChanges();
            //var validationErrors = this.GetValidationErrors();
            //this.ChangeTracker.DetectChanges();

            //if (validationErrors.Any())
            //{
            //    throw new EntityValidationException { ValidationErrors = validationErrors };
            //}

            try
            {
                return base.SaveChanges();
            }
            catch (Exception)
            {
                this.RevertChanges();
                throw;
            }
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new SeriesMap());
            modelBuilder.Configurations.Add(new ArgumentMap());
            modelBuilder.Configurations.Add(new ArgumentValueMap());
            modelBuilder.Configurations.Add(new ExperimentMap());

            //var typesToRegister =
            //    Assembly.GetExecutingAssembly().GetTypes().Where(t => t.BaseType != null
            //        && t.BaseType.IsGenericType
            //        && t.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

            //foreach (var type in typesToRegister)
            //{
            //    dynamic instance = Activator.CreateInstance(type);
            //    modelBuilder.Configurations.Add(instance);
            //}
        }

        public void RevertChanges()
        {
            var entries = this.ChangeTracker.Entries().ToList();
            foreach (var entry in entries)
            {
                this.RevertChange(entry);
            }
        }

        private void RevertChange(DbEntityEntry entry)
        {
            switch (entry.State)
            {
                case EntityState.Added:
                    entry.State = EntityState.Detached;
                    break;
                case EntityState.Modified:
                    entry.Reload();
                    break;
                case EntityState.Unchanged:
                    break;
                default:
                    throw new ArgumentException(entry.State.ToString());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only
                }

                // release any unmanaged objects
                // set object references to null

                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }
}