﻿using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using SocialCirclesPortal.Domain.Entities.Models;
using SocialCirclesPortal.Domain.Services.Interfaces;
using SocialCirclesPortal.Infrastructure.Web.Mapping;

namespace SocialCirclesPortal.Web.Controllers.Api
{
    [RoutePrefix("api/series")]
    public class SeriesController : ApiController
    {
        private readonly IMapperResolver _mapperResolver;

        private readonly ISeriesService _seriesService;

        public SeriesController(IMapperResolver mapperResolver, ISeriesService seriesService)
        {
            _seriesService = seriesService;
            _mapperResolver = mapperResolver;
        }

        [HttpGet]
        public IHttpActionResult GetSeries()
        {
            try
            {
                var series = _seriesService.Queryable().ToList();
                return Ok(_mapperResolver.Map<Models.Series[]>(series));
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("An error occurred while retrieving series. {0}", ex));
            }
        }

        [HttpGet]
        [Route("{seriesId:int}")]
        public IHttpActionResult GetSeriesById(int seriesId)
        {
            try
            {
                Series series = _seriesService.Find(seriesId);
                if (series == null)
                {
                    return NotFound();
                }

                return Ok(_mapperResolver.Map<Models.Series>(series));
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("An error occurred while retrieving series. {0}", ex));
            }
        }

        [HttpPost]
        public IHttpActionResult AddSeries(Models.Series series)
        {
            try
            {
                _seriesService.AddSeries(_mapperResolver.Map<Series>(series));
                return Ok(series);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("An error occurred while adding new series. {0}", ex));
            }
        }

        [HttpPut]
        public IHttpActionResult UpdateSeries(Series series)
        {
            try
            {
                _seriesService.UpdateSeries(series);
                return Ok(series);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("An error occurred while updating series. {0}", ex));
            }
        }

        [HttpPut]
        [Route("execute/{seriesId:int}")]
        public IHttpActionResult ExecuteSeries(int seriesId)
        {
            return null;            
        }

        [HttpDelete]
        [Route("series{seriesId:int}")]
        public IHttpActionResult DeleteSeries(int seriesId)
        {
            try
            {
                _seriesService.DeleteSeries(seriesId);
                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("An error occurred while updating series. {0}", ex));
            }
        }
    }
}