﻿using System.Linq;
using System.Web.Mvc;
using SocialCirclesPortal.Domain.Services.Interfaces;
using SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner;
using SocialCirclesPortal.Infrastructure.Web.ConfigurationProvider;


namespace SocialCirclesPortal.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISeriesService _seriesService;
        private readonly IAlgorithmRunner _algorithmRunner;
        private readonly IConfigurationProvider _configurationProvider;

        public HomeController(ISeriesService seriesService, IAlgorithmRunner algorithmRunner, IConfigurationProvider configurationProvider)
        {
            _seriesService = seriesService;
            _algorithmRunner = algorithmRunner;
            _configurationProvider = configurationProvider;
        }

        public ActionResult Index()
        {
            var result = _seriesService.Queryable().ToList();

            var arguments = new SnapAlgorithmArguments
            {
                Ego = string.Format("{0}/facebook/698", _configurationProvider.AlgorithmDatabasesPath),
                K = "3",
                Lambda = "1",
                Training = "25",
                Gradient = "50",
                Improve = "5",
                ResultFilePath = _configurationProvider.AlgorithmResultsPath
            };

            //_algorithmRunner.RunAlgorithm(arguments);

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}