﻿using System;
using System.Linq;
using AutoMapper;
using SocialCirclesPortal.Domain.Entities.Models;
using SocialCirclesPortal.Infrastructure.Web.Mapping;

namespace SocialCirclesPortal.Web.Models
{
    public class Series : IHaveCustomMappings
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreateDateTime { get; set; }

        public DateTime? ChangeDateTime { get; set; }

        public Argument K { get; set; }

        public Argument Restarts { get; set; }

        public Argument Lambda { get; set; }

        public Argument Gradient { get; set; }

        public Argument Improve { get; set; }

        public Argument Training { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            // Series -> SeriesDto
            configuration.CreateMap<Domain.Entities.Models.Series, Series>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.K, opts => opts.MapFrom(src => src.Arguments.First(x => x.ArgumentType == ArgumentType.K)))
                .ForMember(dest => dest.Restarts, opts => opts.MapFrom(src => src.Arguments.First(x => x.ArgumentType == ArgumentType.Restarts)))
                .ForMember(dest => dest.Lambda, opts => opts.MapFrom(src => src.Arguments.First(x => x.ArgumentType == ArgumentType.Lambda)))
                .ForMember(dest => dest.Gradient, opts => opts.MapFrom(src => src.Arguments.First(x => x.ArgumentType == ArgumentType.Gradient)))
                .ForMember(dest => dest.Improve, opts => opts.MapFrom(src => src.Arguments.First(x => x.ArgumentType == ArgumentType.Improve)))
                .ForMember(dest => dest.Training, opts => opts.MapFrom(src => src.Arguments.First(x => x.ArgumentType == ArgumentType.Training)))
                .ForMember(dest => dest.CreateDateTime, opts => opts.MapFrom(src => src.CreateDateTime))
                .ForMember(dest => dest.ChangeDateTime, opts => opts.MapFrom(src => src.ChangeDateTime));

            // SeriesDto -> Series
            configuration.CreateMap<Series, Domain.Entities.Models.Series>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.CreateDateTime, opts => opts.MapFrom(src => src.CreateDateTime))
                .ForMember(dest => dest.ChangeDateTime, opts => opts.MapFrom(src => src.ChangeDateTime))
                .AfterMap((s, d) => d.Arguments.Add(Mapper.Map<Domain.Entities.Models.Argument>(s.Restarts)))
                .AfterMap((s, d) => d.Arguments.Add(Mapper.Map<Domain.Entities.Models.Argument>(s.K)))
                .AfterMap((s, d) => d.Arguments.Add(Mapper.Map<Domain.Entities.Models.Argument>(s.Lambda)))
                .AfterMap((s, d) => d.Arguments.Add(Mapper.Map<Domain.Entities.Models.Argument>(s.Gradient)))
                .AfterMap((s, d) => d.Arguments.Add(Mapper.Map<Domain.Entities.Models.Argument>(s.Improve)))
                .AfterMap((s, d) => d.Arguments.Add(Mapper.Map<Domain.Entities.Models.Argument>(s.Training)));
        }
    }
}