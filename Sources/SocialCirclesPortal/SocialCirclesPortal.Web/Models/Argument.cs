﻿using System;
using AutoMapper;
using SocialCirclesPortal.Domain.Entities.Models;
using SocialCirclesPortal.Infrastructure.Web.Mapping;

namespace SocialCirclesPortal.Web.Models
{
    public class Argument : IHaveCustomMappings
    {
        public int Id { get; set; }

        public int Min { get; set; }

        public int Max { get; set; }

        public int Step { get; set; }

        public int From { get; set; }

        public int To { get; set; }

        public string ArgumentType { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            // Argument -> ArgumentDto
            configuration.CreateMap<Domain.Entities.Models.Argument, Argument>()
                .ForMember(dest => dest.Min, opts => opts.MapFrom(src => src.Min))
                .ForMember(dest => dest.Max, opts => opts.MapFrom(src => src.Max))
                .ForMember(dest => dest.Step, opts => opts.MapFrom(src => src.Step))
                .ForMember(dest => dest.From, opts => opts.MapFrom(src => src.From))
                .ForMember(dest => dest.To, opts => opts.MapFrom(src => src.To))
                .ForMember(dest => dest.ArgumentType, opts => opts.MapFrom(src => src.ArgumentType.ToString()));

            // ArgumentDto -> Argument
            configuration.CreateMap<Argument, Domain.Entities.Models.Argument>()
                .ForMember(dest => dest.Min, opts => opts.MapFrom(src => src.Min))
                .ForMember(dest => dest.Max, opts => opts.MapFrom(src => src.Max))
                .ForMember(dest => dest.Step, opts => opts.MapFrom(src => src.Step))
                .ForMember(dest => dest.From, opts => opts.MapFrom(src => src.From))
                .ForMember(dest => dest.To, opts => opts.MapFrom(src => src.To))
                .ForMember(dest => dest.ArgumentType, opts => opts.MapFrom(src => Enum.Parse(typeof(ArgumentType), src.ArgumentType)));
        }
    }
}