﻿var config = {
    bowerPath: './bower_components',
    nodePath: './node_modules',
    clientPath: './Client',
    clientPathApp: './Client/app',
    clientPathCss: './Client/css',
    clientPathCssVendors: './Client/css/vendors',
    clientPathScripts: './Client/scripts'
};

var gulp = require('gulp');

// Include plugins
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files', 'run-sequence'],
    replaceString: /\bgulp[\-.]/
});

// Install bower components
gulp.task('bower', function () {
    return plugins.bower({ layout: "byComponent" });
});

// Fonts
gulp.task('fonts', function () {
    // copy fonts
    return gulp.src([
            config.bowerPath + '/fontawesome/fonts/*.*'
    ])
        .pipe(plugins.plumber())
        .pipe(gulp.dest(config.clientPath + '/fonts/'));
});

// Copy main js files from bower_components
gulp.task('copy-bower-js', function () {
    return gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.plumber())
        .pipe(plugins.filter('*.js'))
        .pipe(gulp.dest(config.clientPathScripts + '/vendors/'));
});

// Copy main css files from bower_components
gulp.task('copy-bower-css', function () {
    return gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.plumber())
        .pipe(plugins.filter('*.css'))
        .pipe(gulp.dest(config.clientPathCssVendors));
});

// Minify and concat css files to styles.css file
gulp.task('styles', function () {
    return gulp.src([
            config.clientPathCssVendors + '/bootstrap.css',
            config.clientPathCssVendors + '/font-awesome.css',
            config.clientPathCssVendors + '/metisMenu.css',
            config.clientPathCssVendors + '/footable.core.css',
            config.clientPathCssVendors + '/ion.rangeSlider.css',
            config.clientPathCssVendors + '/ion.rangeSlider.skinFlat.css',
            config.clientPathCssVendors + '/animate.css',
            config.clientPathCss + '/main.css'
    ])
        .pipe(plugins.plumber())
        .pipe(plugins.minifyCss())
        .pipe(plugins.concat('styles.min.css')).pipe(gulp.dest(config.clientPathCss));
});

// Minify and concat js files  to scripts.js file
gulp.task('scripts-common', function () {
    return gulp.src([
            config.clientPathScripts + '/vendors/jquery.js',
            config.clientPathScripts + '/vendors/bootstrap.js',
            config.clientPathScripts + '/vendors/metisMenu.js',
            config.clientPathScripts + '/vendors/pace.js',
            config.clientPathScripts + '/vendors/jquery.slimscroll.min.js',
            config.clientPathScripts + '/vendors/footable.js',
            config.clientPathScripts + '/vendors/ion.rangeSlider.js',
            config.clientPathScripts + '/inspinia.js'
    ])
        .pipe(plugins.plumber())
        .pipe(plugins.uglify({ mangle: false }))
        .pipe(plugins.concat('scripts.common.min.js')).pipe(gulp.dest(config.clientPathScripts));
});

// Minify and concat js files to scripts.js file
gulp.task('scripts-angular', function () {
    return gulp.src([
            config.clientPathScripts + '/vendors/angular.js',
            config.clientPathScripts + '/vendors/angular-route.js',
            config.clientPathScripts + '/vendors/angular-resource.js',
            config.clientPathScripts + '/vendors/angular-footable.js'
    ])
        .pipe(plugins.plumber())
        .pipe(plugins.uglify({ mangle: false }))
        .pipe(plugins.concat('scripts.angular.min.js')).pipe(gulp.dest(config.clientPathScripts));
});

// Log main bower files to console
gulp.task('show', function () {
    console.log(plugins.mainBowerFiles().map(function (path) {
        return path.replace(/^.*[\\\/]/, '');
    }));
});

// Removes node and bower packages
gulp.task('clean', function () {
    return gulp.src([
        config.bowerPath,
        config.nodePath
    ]).pipe(plugins.plumber()).pipe(plugins.clean({ force: true }));
});

gulp.task('build', function () {
    plugins.runSequence('bower', 'fonts', 'copy-bower-js', 'copy-bower-css', 'styles', 'scripts-common', 'scripts-angular');
});

gulp.task('watch', function () {
    // Watch .css files
    gulp.watch(config.clientPathCss + '/**/*.css', ['styles']);
});

gulp.task('default', ['build']);