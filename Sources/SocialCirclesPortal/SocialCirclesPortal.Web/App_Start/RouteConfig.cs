﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SocialCirclesPortal.Web
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "dashboard",
                url: "dashboard/{*catchall}",
                defaults: new { controller = "dashboard", action = "index" });

            routes.MapRoute(
                name: "series",
                url: "series/{*catchall}",
                defaults: new { controller = "series", action = "index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
        }
    }
}
