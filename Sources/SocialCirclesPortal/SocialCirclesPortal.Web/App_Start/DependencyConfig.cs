﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using SocialCirclesPortal.Domain.Entities.Models;
using SocialCirclesPortal.Domain.Repository;
using SocialCirclesPortal.Domain.Services.DomainServices;
using SocialCirclesPortal.Domain.Services.Interfaces;
using SocialCirclesPortal.Infrastructure.DataAccess;
using SocialCirclesPortal.Infrastructure.Web.AlgorithmRunner;
using SocialCirclesPortal.Infrastructure.Web.Algorithms;
using SocialCirclesPortal.Infrastructure.Web.ApplicationPathResolver;
using SocialCirclesPortal.Infrastructure.Web.BackgroundTaskRunner;
using SocialCirclesPortal.Infrastructure.Web.ConfigurationProvider;
using SocialCirclesPortal.Infrastructure.Web.Mapping;
using SocialCirclesPortal.Infrastructure.Web.ProcessRunner;

namespace SocialCirclesPortal.Web
{
    public class DependencyConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            // Infrastructure
            builder.RegisterType<MapperResolver>().As<IMapperResolver>().InstancePerRequest();
            builder.RegisterType<BackgroundTaskRunner>().As<IBackgroundTaskRunner>().InstancePerRequest();
            builder.RegisterType<AlgorithmRunner>().As<IAlgorithmRunner>();
            builder.RegisterType<ProcessRunner>().As<IProcessRunner>();
            builder.RegisterType<ConfigurationProvider>().As<IConfigurationProvider>();
            builder.RegisterType<ApplicationPathResolver>().As<IApplicationPathResolver>();

            // Algorithms
            builder.RegisterType<SnapAlgorithm>().Keyed<IAlgorithm>(AlgorithmType.Snap);

            // Register controllers
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register repository
            builder.RegisterType<DatabaseContext>().As<IDataContext>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            // Register services
            builder.RegisterType<SeriesService>().As<ISeriesService>();
            //builder.RegisterAssemblyTypes(typeof (Service<>).Assembly)
            //    .Where(t => t.Name.EndsWith("Service"))
            //    .AsImplementedInterfaces().InstancePerRequest();

            // Set the dependency resolver to be Autofac.
            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}