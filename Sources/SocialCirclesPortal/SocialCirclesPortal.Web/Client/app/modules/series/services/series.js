﻿(function () {
    'use strict';

    function seriesService(apiHelper) {
        this.getSeries = function () {
            return apiHelper.get('api/series').then(function (data) {
                return data;
            });
        };

        this.getSeriesById = function (seriesId) {
            return apiHelper.get('api/series/' + seriesId).then(function (data) {
                return data;
            });
        };

        this.saveSeries = function (series) {
            return series.Id ? this.updateSeries(series) : this.addSeries(series);
        };

        this.updateSeries = function (series) {
            return apiHelper.put('api/series', series).then(function (data) {
                return data;
            });
        };

        this.addSeries = function (series) {
            return apiHelper.post('api/series', series).then(function (data) {
                return data;
            });
        };
    }

    seriesService.$inject = ['apiHelper'];
    angular.module('app.series').service('seriesService', seriesService);
}());
