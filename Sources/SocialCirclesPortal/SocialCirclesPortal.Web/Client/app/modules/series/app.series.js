﻿(function () {
    'use strict';

    angular.module('app.series').
        config(function ($routeProvider, $locationProvider) {
            $routeProvider.when('/series', { templateUrl: 'Client/app/modules/series/views/seriesList.html', controller: 'SeriesListController', controllerAs: 'vm', label: 'Series' });
            $routeProvider.when('/series/details/:seriesId?', { templateUrl: 'Client/app/modules/series/views/seriesDetails.html', controller: 'SeriesDetailsController', controllerAs: 'vm', label: 'Series Details' });
            $routeProvider.otherwise({ redirectTo: '/series' });

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        });
}());