﻿(function (socialCirclesPortal) {
    'use strict';

    function SeriesListController(seriesService, apiHelper) {
        var vm = this;
        vm.series = {};

        vm.addSeries = function () {
            apiHelper.navigateTo('series/details');
        };

        vm.updateSeries = function (series) {
            apiHelper.navigateTo('series/details/' + series.id);
        };

        vm.initialize = function () {
            seriesService.getSeries().then(function (data) {
                vm.series = data;
            });
        };

        vm.initialize();
    }

    SeriesListController.$inject = ['seriesService', 'apiHelper'];
    angular.module('app.series').controller('SeriesListController', SeriesListController);
}(window.SocialCirclesPortal));
