﻿(function () {
    'use strict';

    function SeriesDetailsController($routeParams, seriesService, apiHelper) {
        var vm = this;

        function showArgumentConfiguration(argument) {
            vm.editArgument = {
                argumentType: argument.argumentType,
                max: argument.max,
                min: argument.min,
                step: argument.step,
                from: argument.from,
                to: argument.to
            };
        }

        function confirmArgumenttConfiguration(editArgument) {
            vm.series[editArgument.argumentType.toLowerCase()] = editArgument;
        }

        function saveSeries() {
            seriesService.saveSeries(vm.series).then(function () {
                // success 
            });
        }

        function cancelSeries() {
            apiHelper.navigateTo('series');
        }

        function initialize() {
            vm.series = {
                name: '',
                k: { argumentType: 'K', min: 0, max: 10, step: 1, from: 3, to: 4 },
                lambda: { argumentType: 'Lambda', min: 0, max: 10, step: 1, from: 1, to: 2 },
                training: { argumentType: 'Training', min: 20, max: 30, step: 1, from: 25, to: 26 },
                gradient: { argumentType: 'Gradient', min: 50, max: 55, step: 1, from: 50, to: 51 },
                improve: { argumentType: 'Improve', min: 0, max: 10, step: 1, from: 5, to: 6 },
                iterations: { argumentType: 'Restarts', min: 1, max: 30, step: 1, from: 5, to: 5}
            };

            if ($routeParams.seriesId) {
                seriesService.getSeriesById($routeParams.seriesId).then(function (data) {
                    vm.series = data;
                });
            }
        }

        vm.saveSeries = saveSeries;
        vm.cancelSeries = cancelSeries;
        vm.showArgumentConfiguration = showArgumentConfiguration;
        vm.confirmArgumenttConfiguration = confirmArgumenttConfiguration;
        vm.initialize = initialize;

        vm.initialize();
    }

    SeriesDetailsController.$inject = ['$routeParams', 'seriesService', 'apiHelper'];
    angular.module('app.series').controller('SeriesDetailsController', SeriesDetailsController);
}());