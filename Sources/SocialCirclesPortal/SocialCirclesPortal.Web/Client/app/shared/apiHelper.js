﻿(function (socialCirclesPortal) {
    'use strict';

    socialCirclesPortal.apiHelper = function ($http, $q, $window, $location) {
        var self = {};

        self.get = function (uri, data) {
            return $http.get(socialCirclesPortal.rootPath + uri, data).then(function (result) {
                if (typeof result.data === 'object') {
                    return result.data;
                }

                // invalid result
                return $q.reject(result.data);
            }, function (result) {
                // something went wrong
                return $q.reject(result.data);
            });
        };

        self.post = function (uri, data) {
            return $http.post(socialCirclesPortal.rootPath + uri, data).then(function (result) {
                if (typeof result.data === 'object') {
                    return result.data;
                }

                // invalid result
                return $q.reject(result.data);
            }, function (result) {
                // something went wrong
                return $q.reject(result.data);
            });
        };

        self.put = function (uri, data) {
            return $http.put(socialCirclesPortal.rootPath + uri, data).then(function (result) {
                if (typeof result.data === 'object') {
                    return result.data;
                }

                // invalid result
                return $q.reject(result.data);
            }, function (result) {
                // something went wrong
                return $q.reject(result.data);
            });
        };

        self.goBack = function () {
            $window.history.back();
        };

        self.navigateTo = function (path) {
            $location.path(socialCirclesPortal.rootPath + path);
        };

        self.refreshPage = function (path) {
            $window.location.href = socialCirclesPortal.rootPath + path;
        };

        self.clone = function (obj) {
            return JSON.parse(JSON.stringify(obj));
        };

        return self;
    };

    angular.module('app.core').factory('apiHelper', socialCirclesPortal.apiHelper);
}(window.SocialCirclesPortal));
