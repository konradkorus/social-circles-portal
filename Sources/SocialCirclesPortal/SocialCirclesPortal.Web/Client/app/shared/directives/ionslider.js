﻿angular.module('app.directives', []).directive('ionslider', ['$document', function ($document) {
    "use strict";
    return {
        restrict: 'EA',
        require: 'ngModel',
        template: '<input type="text" />',
        scope: {
            ngModel: '='
        },
        link: function (scope, element, attrs, ngModel) {
            var input = element.find('input');
            input.ionRangeSlider({
                grid: true,
                hide_min_max: true,
                values: scope.$eval(attrs.values),
                min_prefix: attrs.minPrefix,
                max_prefix: attrs.maxPrefix,
                min_postfix: attrs.minPostfix,
                max_postfix: attrs.maxPostfix,
                min: +attrs.min,
                max: +attrs.max,
                type: attrs.type,
            });
            var slider = input.data("ionRangeSlider");

            ngModel.$render = function () {
                slider.update(ngModel.$viewValue);
            };
            input.on('change', function () {
                ngModel.$modelValue.from = input.data('from');
                ngModel.$modelValue.to = input.data('to');
            });
            element.on('mousedown', function () {
                if (!scope.$eval(attrs.ngDisabled)) {
                    $document.one('mouseup', function () {
                        scope.$emit('sliderSlideFinished');
                    });
                }
            });
            scope.$watch(attrs.ngDisabled, function (disabled) {
                slider.update({
                    disable: disabled
                });
            });
            scope.$watch(
                function () {
                    return ngModel.$modelValue;
                }, function (value) {
                    slider.update({
                        max: value.max,
                        min: value.min,
                        from: value.from,
                        to: value.to,
                        step: value.step
                    });
                }, true);
            scope.$on('$destroy', function () {
                slider.destroy();
            });
        }
    };
}]);