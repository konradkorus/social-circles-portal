﻿(function () {
    'use strict';

    angular.module('app.core', [
        // angular modlues
        'ngRoute',

        // 3rd-paryt modules
        'ui.footable'
    ]);

    angular.module('app.directives', ['ngRoute']);

    angular.module('app.series', ['app.core', '720kb.tooltips']);

    angular.module('app', [
        // shared modules
        'app.core',
        'app.directives',

        // feature areas
        'app.series'
    ]);
}());
