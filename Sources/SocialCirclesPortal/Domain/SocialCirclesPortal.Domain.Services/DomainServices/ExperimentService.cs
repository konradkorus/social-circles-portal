﻿using SocialCirclesPortal.Domain.Entities.Models;
using SocialCirclesPortal.Domain.Repository;
using SocialCirclesPortal.Domain.Services.Interfaces;

namespace SocialCirclesPortal.Domain.Services.DomainServices
{
    public class ExperimentService : Service<Experiment>, IExperimentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Experiment> _repository;

        public ExperimentService(IUnitOfWork unitOfWork, IRepository<Experiment> repository)
            : base(repository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }
    }
}