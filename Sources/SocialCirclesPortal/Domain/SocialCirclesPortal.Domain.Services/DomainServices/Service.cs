﻿using System.Linq;
using SocialCirclesPortal.Domain.Repository;
using SocialCirclesPortal.Domain.Services.Interfaces;

namespace SocialCirclesPortal.Domain.Services.DomainServices
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;

        public Service(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public TEntity Find(params object[] keyValues)
        {
            return _repository.Find(keyValues);
        }

        public IQueryable<TEntity> Queryable()
        {
            return _repository.Queryable();
        }

        public void Add(TEntity entity)
        {
            _repository.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _repository.Update(entity);
        }

        public void Delete(object id)
        {
            _repository.Delete(id);
        }

        public void Delete(TEntity entity)
        {
            _repository.Delete(entity);
        }
    }
}