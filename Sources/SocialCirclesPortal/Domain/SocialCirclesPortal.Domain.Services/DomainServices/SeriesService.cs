﻿using System;
using SocialCirclesPortal.Domain.Entities.Models;
using SocialCirclesPortal.Domain.Repository;
using SocialCirclesPortal.Domain.Services.Interfaces;

namespace SocialCirclesPortal.Domain.Services.DomainServices
{
    public class SeriesService : Service<Series>, ISeriesService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRepository<Series> _repository;

        public SeriesService(IUnitOfWork unitOfWork, IRepository<Series> repository)
            : base(repository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public Series GetSeriesById(int id)
        {
            return Find(id);
        }

        public bool AddSeries(Series series)
        {
            series.CreateDateTime = DateTime.UtcNow;
            series.SetArguments();

            _repository.Add(series);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool UpdateSeries(Series series)
        {
            series.ChangeDateTime = DateTime.UtcNow;
            series.SetArguments();

            _repository.Update(series);
            _unitOfWork.SaveChanges();

            return true;
        }

        public bool DeleteSeries(int seriesId)
        {
            _repository.Delete(seriesId);
            return true;
        }
    }
}