﻿using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Domain.Services.Interfaces
{
    public interface IExperimentService : IService<Experiment>
    {
         
    }
}