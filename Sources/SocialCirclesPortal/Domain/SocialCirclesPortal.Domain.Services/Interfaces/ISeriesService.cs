﻿using SocialCirclesPortal.Domain.Entities.Models;

namespace SocialCirclesPortal.Domain.Services.Interfaces
{
    public interface ISeriesService : IService<Series>
    {
        Series GetSeriesById(int id);

        bool AddSeries(Series series);

        bool UpdateSeries(Series series);

        bool DeleteSeries(int seriesId);
    }
}