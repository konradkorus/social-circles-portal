using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace SocialCirclesPortal.Domain.Repository
{
    public interface IDataContext : IDisposable
    {
        int SaveChanges();

        void RevertChanges();

        IDbSet<TEntity> GetEntitySet<TEntity>() where TEntity : class;

        DbQuery<TEntity> GetDbQuery<TEntity>() where TEntity : class;

        void ChangeState<TEntity>(TEntity entity, EntityState entityState) where TEntity : class;
    }
}