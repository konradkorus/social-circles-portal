using System;
using System.Data;

namespace SocialCirclesPortal.Domain.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();

        bool Commit();

        void Rollback();

        void Dispose(bool disposing);

        IRepository<TEntity> Repository<TEntity>() where TEntity : class;

        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
    }
}