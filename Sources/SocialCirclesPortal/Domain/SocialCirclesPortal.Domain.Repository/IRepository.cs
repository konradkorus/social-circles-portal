using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SocialCirclesPortal.Domain.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Find(params object[] keyValues);

        IQueryable<TEntity> Queryable();

        /// <summary>
        ///     Gets the current context.
        /// </summary>
        /// <returns>The context.</returns>
        IDataContext Context { get; }

        /// <summary>
        ///     Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Add(TEntity entity);

        /// <summary>
        ///     Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes entity based on the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(object id);

        /// <summary>
        ///     Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Delete(TEntity entity);

        /// <summary>
        ///     Gets all.
        /// </summary>
        /// <returns>Returns all entities from repository.</returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        ///     Finds entities by the specified predicate.
        /// </summary>
        /// <param name="wherePredicate">The where predicate.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>All entities meeting the given search criteria.</returns>
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> wherePredicate, params Expression<Func<TEntity, object>>[] includes);

        IQueryable<TEntity> Select(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            int? page = null,
            int? pageSize = null);
    }
}