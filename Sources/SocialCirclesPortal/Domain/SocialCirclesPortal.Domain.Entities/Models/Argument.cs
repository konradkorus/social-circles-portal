﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SocialCirclesPortal.Domain.Entities.Models
{
    public class Argument : Entity<int>
    {
        public Argument()
        {
            Values = new Collection<ArgumentValue>();
        }

        public int Min { get; set; }

        public int Max { get; set; }

        public int Step { get; set; }

        public int From { get; set; }

        public int To { get; set; }

        public ArgumentType ArgumentType { get; set; }

        public Series Series { get; set; }

        public virtual ICollection<ArgumentValue> Values { get; set; }

        public void AddArgumentValue(int value)
        {
            Values.Add(new ArgumentValue
            {
                Value = value,
                Argument = this
            });
        }

        public void SetArgumentValues()
        {
            var values = Enumerable.Range(From, To - From + 1).Where(x => x % Step == 0);
            foreach (var value in values)
            {
                AddArgumentValue(value);
            }
        }
    }
}