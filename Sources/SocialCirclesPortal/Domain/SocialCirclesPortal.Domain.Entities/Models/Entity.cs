﻿namespace SocialCirclesPortal.Domain.Entities.Models
{
    public class Entity<TKey>
    {
        public TKey Id { get; set; }
    }
}
