﻿namespace SocialCirclesPortal.Domain.Entities.Models
{
    public enum ArgumentType
    {
        /// <summary>
        /// Number of circles.
        /// </summary>
        K = 1,

        /// <summary>
        /// Regularization parameter.
        /// </summary>
        Lambda = 2,

        /// <summary>
        /// Number of iterations of training.
        /// </summary>
        Training = 3,

        /// <summary>
        /// Number of iterations of gradient ascent.
        /// </summary>
        Gradient = 4,

        /// <summary>
        /// Number of iterations using by QPBO.
        /// </summary>
        Improve = 5,

        /// <summary>
        /// Number of random restarts.
        /// </summary>
        Restarts = 6
    }
}