﻿namespace SocialCirclesPortal.Domain.Entities.Models
{
    public class Experiment : Entity<int>
    {
        public string Result1 { get; set; }
        public string Result2 { get; set; }
    }
}