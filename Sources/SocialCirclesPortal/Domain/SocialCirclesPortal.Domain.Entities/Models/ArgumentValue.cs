﻿namespace SocialCirclesPortal.Domain.Entities.Models
{
    public class ArgumentValue : Entity<int>
    {
        public int Value { get; set; }

        //public int ArgumentId { get; set; }

        public Argument Argument { get; set; }
    }
}