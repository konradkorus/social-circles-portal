﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SocialCirclesPortal.Domain.Entities.Models
{
    public class Series : Entity<int>
    {
        public Series()
        {
            Arguments = new Collection<Argument>();
        }

        public string Name { get; set; }

        public AlgorithmType AlgorithmType { get; set; }

        public DateTime CreateDateTime { get; set; }

        public DateTime? ChangeDateTime { get; set; }

        public virtual ICollection<Argument> Arguments { get; set; }

        public void SetArguments()
        {
            foreach (var argument in Arguments)
            {
                argument.SetArgumentValues();
            }
        }
    }
}