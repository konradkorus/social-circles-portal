// Circles.App.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <algorithm>   
#include <vector>
#include <sstream>
#include <string>
#include <set>
#include <chrono>
#include <string>
using namespace std;

vector<int>intersection(vector<int>&, vector<int>&);
vector<int> remove_double_features(vector<int>);
bool same_vector(vector<int> A, vector<int> B);
void debug_f(vector<vector<int>> F);
void print_f(vector<vector<int>> F);
vector<int> load_ego(string name);
void load_matrix(string name, vector<vector<int>> &nodes, vector<int> &users);
vector<vector<int> > load_circles_matrix(string name);
void find_circles_multiple(vector<vector<int>> &F, vector<vector<int> > &circles, vector<int> &No, int maxNumOfIterations, vector<vector<int> > &realCircles, vector<int> &Mo);
void lets_check(vector<vector<int>> circles);
void find_circles_one(vector<vector<int>> &F, vector<vector<int> > &circles, vector<int> &No, vector<vector<int>> nodes, vector<int> ego, vector<int> users, int maxNumOfIterations, vector<vector<int> > &realCircles, vector<int> &Mo);
void print_no(vector<int> No);
vector<int> make_mo(vector<vector<int>> groundTruthCircles);
vector<int> make_Wo(vector<vector<int> > similarCircles);
void write_circles(vector<vector<int>> circles, string name);
double F_Score(vector<vector<int> > &circles, vector<vector<int> > &realCircles, vector<int> &No, vector<int> &Mo);
double F_Score_similarity(vector<vector<int> > &similarCircles, vector<vector<int> > &realCircles, vector<int> &Mo, vector<int> &Wo);
//vector<vector<int>> find_circles_similarity(vector<vector<int>> nodes, vector<int> users, vector<int> ego, double threshold, double coeffWeight_1, double coeffWeight_0);
void find_similar_circles_one(vector<vector<int>> &F, vector<vector<int> > &circles, vector<vector<int>> nodes, vector<int> ego, vector<int> users, int maxNumOfIterations, double trashhold, double higerW, double lowerW);
void find_similar_circles_multiple(vector<vector<int>> &F, vector<vector<int> > &circles, vector<int> &ego, int maxNumOfIterations);

int _tmain(int argc, _TCHAR* argv[])
{
	chrono::steady_clock::time_point begin = chrono::steady_clock::now();
	if (argc != 3) return 1;

	vector<int> ego, users, No, Mo, Wo;
	vector<vector<int>> nodes, circles, F, groundTruthCircles;
	ego = load_ego(string(argv[1]));
	load_matrix(string(argv[1]), nodes, users);
	groundTruthCircles = load_circles_matrix(string(argv[1]));
	Mo = make_mo(groundTruthCircles);
	// code for making array of all users (friends) of ego

	find_circles_one(F, circles, No, nodes, ego, users, stoi(argv[2]), groundTruthCircles, Mo);
	print_f(F);
	print_no(No);

	//Finding circles based on more common features //
	find_circles_multiple(F, circles, No, stoi(argv[2]), groundTruthCircles, Mo);
	write_circles(circles, "circles.txt");
	debug_f(F);

	//Finding FScore
	double fScore = F_Score(circles, groundTruthCircles, No, Mo);

	//Similarity circles
	//similarityCircles = find_circles_similarity(nodes, users, ego, 7, 0.8, 0.5);
	vector<vector<int>> similarCircles, similarF;
	//cout << "Find circles based on similarity" << endl;
	//find_similar_circles_one(similarF, similarCircles, nodes, ego, users, stoi(argv[2]), 0, 0.7, 0.2);
	//cout << "Print similar F" << endl;
	//find_similar_circles_multiple(similarF, similarCircles, ego, stoi(argv[2]));
	Wo = make_Wo(similarCircles);
	//cout << "Print similar F" << endl;
	//print_f(similarF);
	//write_circles(circles, "similarCircles.txt");

	//Finding FScore - with similarity cirlces
	//double fScore_similarity = F_Score_similarity(similarCircles, groundTruthCircles, Mo, Wo);
	//cout << "Fscore similar" << fScore_similarity << endl;
	chrono::steady_clock::time_point end = chrono::steady_clock::now();
	__int64 diff = chrono::duration_cast<chrono::microseconds>(end - begin).count();
	ofstream results_time("time.txt");
	results_time << "Time difference = " << diff / 1000000 << " s " << (diff % 1000000) / 1000 << " ms" << "\n";
	//system("pause");
	return 0;
}


// Definition of a function intersection 
vector<int> intersection(vector<int>& A, vector<int>& B){

	vector<int> v3;

	/**
	//Assume that we have sorted earlier
	sort(A.begin(), A.end());
	sort(B.begin(), B.end());
	*/
	set_intersection(A.begin(), A.end(), B.begin(), B.end(), back_inserter(v3));

	return v3;
}

vector<int> remove_double_features(vector<int> A){

	std::sort(A.begin(), A.end());
	auto last = std::unique(A.begin(), A.end());
	A.erase(last, A.end());

	return A;
}

bool same_vector(vector<int> A, vector<int> B){

	bool bEqual = true;
	if (A.size() == B.size()){
		if (!std::equal(A.begin(), A.begin() + A.size(), B.begin())){
			bEqual = false;
		}
	}
	else {
		bEqual = false;
	}

	return bEqual;
}

void debug_f(vector<vector<int>> F){
	ofstream test("sandra.txt");
	test << "Niz F: " << endl;
	for (int i = 0; i < F.size(); i++) {
		vector<int> T = F[i];
		for (int j = 0; j < T.size(); j++){
			test << T[j] << " ";
		}
		test << "\n";
	}
	test << "\n";
}

void print_f(vector<vector<int>> F){
	// Print values in the array F
	cout << "Positions of features size: " << F.size() << ":" << endl;
	for (int i = 0; i < F.size(); i++) {
		vector<int> T = F[i];

		for (int j = 0; j < T.size(); j++){
			cout << T[j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

vector<int> load_ego(string name){
	// Open file
	ifstream egoFile(name + ".egofeat"); //otvara file facebook/ ...

	// Read from file each number and insert in array
	int number;
	vector<int> ego;

	if (egoFile.is_open()){
		while (egoFile >> number){
			ego.push_back(number); // number is temporary variable to put to ego array from file
		}
	}

	// Print values in the array
	cout << "ego:";
	for (int i = 0; i < ego.size(); i++) {
		cout << ego[i] << " ";
	}
	cout << endl << endl;

	return ego;
}

void load_matrix(string name, vector<vector<int>> &nodes, vector<int> &users){
	// Open file
	ifstream egoFileMatrix(name + ".feat"); //otvara file facebook/ ...

	string line;

	while (!std::getline(egoFileMatrix, line, '\n').eof()) {
		istringstream reader(line); // reading line by line
		vector<int> lineData; // temporary vector that contains one row
		int i = 0;
		while (!reader.eof()) {
			int val;
			reader >> val;

			if (reader.fail())
				break;
			if (i++ != 0){
				lineData.push_back(val);
			}
			else{
				users.push_back(val);
			}
		}
		nodes.push_back(lineData);
	}

	egoFileMatrix.close();

	// printing matrix
	/*	for (int i = 0; i < nodes.size(); i++){
	for (int j = 0; j < nodes[0].size(); j++){
	cout << nodes[i][j] << " ";
	}
	cout << endl;
	}*/
	cout << "\n";


}

vector<vector<int> > load_circles_matrix(string name){
	// Open file
	ifstream circlesFileMatrix(name + ".circles"); //otvara file facebook/ ...
	// Declaration of Matrix and temporary variable line
	vector<vector<int> > realCircles;
	int k;
	string line;

	while (!std::getline(circlesFileMatrix, line, '\n').eof()) {
		istringstream reader(line); // reading line by line
		vector<int> lineData; // temporary vector that contains one row
		k = 0;
		while (!reader.eof()) {
			// Read single word and put it in a temp var
			string val;
			reader >> val;

			if (reader.fail()){
				break;
			}

			if (k != 0){ // for skipping the first word of each line in file
				lineData.push_back(atoi(val.c_str()));
			}

			if (k == 0){
				k = 1;
			}
		}
		realCircles.push_back(lineData);
	}
	circlesFileMatrix.close();

	// printing matrix
	cout << "Circle matrix is: " << endl;
	for (int i = 0; i < realCircles.size(); i++){
		for (int j = 0; j < realCircles[i].size(); j++){
			cout << realCircles[i][j] << " ";
		}
		cout << endl;
	}
	cout << "\n";

	return realCircles;
}



void find_circles_one(vector<vector<int>> &F, vector<vector<int> > &circles, vector<int> &No, vector<vector<int>> nodes, vector<int> ego, vector<int> users, int maxNumOfIterations, vector<vector<int> > &realCircles, vector<int> &Mo){
	chrono::steady_clock::time_point begin = chrono::steady_clock::now();
	// *** Code for Finding Circles based on 1 feature ***//  
	int k;
	ofstream results_features("features.txt");
	for (int j = 0; j < nodes[0].size() && circles.size() < maxNumOfIterations; j++) // iterira kroz svakog korisnika
	{
		std::vector<int> temp;
		vector<int> tempF;
		k = 0;
		if (ego[j] == 1)
		{
			for (int i = 0; i < nodes.size(); i++)
			{
				if (nodes[i][j] == 1)
				{
					temp.push_back(users[i]);
					cout << temp[k] << " ";
					k++;
				}
			}
			cout << "\n";

			if (k != 0)
			{
				sort(temp.begin(), temp.end());
				circles.push_back(temp);
				tempF.push_back(j);
				F.push_back(tempF);
				No.push_back(k);
			}
		}
	}

	cout << endl << endl;
	double fScore = F_Score(circles, realCircles, No, Mo);


	chrono::steady_clock::time_point end = chrono::steady_clock::now();
	__int64 diff = chrono::duration_cast<chrono::microseconds>(end - begin).count();
	ofstream results_time;
	results_time.open("time_new.txt", std::ofstream::out | std::ofstream::app);
	//results_time << diff / 1000 << " " << fScore << "\n";
	results_time << "Time difference za 1 = " << diff / 1000000 << " s " << (diff % 1000000) / 1000 << " ms" << " " << fScore << "\n";

}

void find_circles_multiple(vector<vector<int>> &F, vector<vector<int> > &circles, vector<int> &No, int maxNumOfIterations, vector<vector<int> > &realCircles, vector<int> &Mo){
	int l;
	int NoCircles;

	int lnew = 0;
	int lo = 0; // id number of the starting circle
	set <vector<int>> Fset;
	vector<int> C; // vector as an output of a function
	l = lnew = No.size();
	cout << "Lenght" << l << endl;
	int duplicated = 0;
	int numIterations = 0;
	if (circles.size() >= maxNumOfIterations) {
		return;
	}
	for (int i = 0; i < F.size(); i++){
		Fset.insert(F[i]);
	}
	long printCounter = 0;
	chrono::steady_clock::time_point begin, end;
	__int64 diff;
	ofstream results_time;
	results_time.open("time_new.txt", std::ofstream::out | std::ofstream::app);
	double fScore;

	do{
		begin = chrono::steady_clock::now();
		NoCircles = 0;
		for (int i = lo; i < (l - 1) && circles.size() < maxNumOfIterations; i++){
			for (int j = i + 1; j < l && circles.size() < maxNumOfIterations; j++){
				if (++printCounter % 100000 == 0){
					cout << "Poredim C" << i << "  sa C" << j << endl;
					printCounter = 0;
				}
				vector<int> C = intersection(circles[i], circles[j]);
				if (C.size() > 0){

					vector<int> tempF;
					tempF.insert(tempF.end(), F[i].begin(), F[i].end());
					tempF.insert(tempF.end(), F[j].begin(), F[j].end());
					tempF = remove_double_features(tempF);

					if (Fset.find(tempF) == Fset.end()){

						F.push_back(tempF);
						Fset.insert(tempF);
						circles.push_back(C);
						No.push_back(C.size());
						lnew++;
						NoCircles++;
						numIterations++;

					}

				}

			}


		}
		lo = l;
		l = lnew;
		//lets_check(circles);
		fScore = F_Score(circles, realCircles, No, Mo);


		end = chrono::steady_clock::now();
		diff = chrono::duration_cast<chrono::microseconds>(end - begin).count();
		//results_time << diff / 1000 << " " << fScore2 << "\n";
		results_time << "Time difference = " << diff / 1000000 << " s " << (diff % 1000000) / 1000 << " ms" << " " << fScore << "\n";
	} while (NoCircles > 0);

}

void lets_check(vector<vector<int>> circles){
	cout << "Lets check: " << endl;
	for (int i = 0; i < circles.size(); i++){
		for (int j = 0; j < circles[i].size(); j++){
			cout << circles[i][j] << " ";
		}
		cout << "\n";
	}
}

void write_circles(vector<vector<int>> circles, string name){
	ofstream results(name); //opening an output stream for file circles.txt	
	//Printing circles into a file
	for (int i = 0; i < circles.size(); i++){
		for (int j = 0; j < circles[i].size(); j++){
			results << circles[i][j] << " ";
		}
		results << "\n";
	}

	results.close();
}

void print_no(vector<int> No){
	cout << "Number of elements in each row (circle): " << endl;
	for (int i = 0; i < No.size(); i++) {
		cout << No[i] << " ";
	}
	cout << endl;
}

vector<int> make_mo(vector<vector<int> > realCircles){
	vector<int> Mo;
	int k;
	Mo.clear();
	for (int i = 0; i < realCircles.size(); i++){
		k = 0;
		for (int j = 0; j < realCircles[i].size(); j++){
			k++;
		}
		Mo.push_back(k);
	}
	cout << "Number of elements in each row in real data(circle): " << endl;
	for (int i = 0; i < Mo.size(); i++) {
		cout << Mo[i] << " ";
	}
	cout << endl;

	return Mo;
}

double F_Score(vector<vector<int> > &circles, vector<vector<int> > &realCircles, vector<int> &No, vector<int> &Mo){
	double fScoreAvg = 0;
	vector <int> predicted, actual;
	vector <double>    fScoresPerOne;
	int No_True_Positive;
	double recall, precision, fMax;
	int n, m;
	vector<double> fMaxPerOnePredicted;
	ofstream results_fScore("fscore.txt");

	for (int i = 0; i < circles.size(); i++){
		//fScoresPerOne.push_back(0);

		predicted.clear();
		n = No[i];
		for (int k = 0; k < n; k++){
			int val = circles[i][k];
			predicted.push_back(val);
		}
		//results_fScore << "Predicted " << i << ": " << "\n";
		for (int j = 0; j < realCircles.size(); j++){
			actual.clear();
			m = Mo[j];
			for (int k = 0; k < m; k++){
				int val = realCircles[j][k];
				actual.push_back(val);
			}

			int TruePositive = 0;
			int FalsePositive = 0;
			int FalseNegative = 0;
			bool match = false;

			for (int p = 0; p < predicted.size(); p++){
				match = false;
				for (int k = 0; k < actual.size(); k++){
					if (predicted[p] == actual[k]){
						match = true;
						break;
					}
				}
				if (match){
					TruePositive++;
				}
				else{
					FalsePositive++;
				}
			}

			for (int k = 0; k < actual.size(); k++){
				match = false;
				for (int p = 0; p < predicted.size(); p++){
					if (actual[k] == predicted[p]){
						match = true;
						break;
					}
				}
				if (!match){
					FalseNegative++;
				}
			}

			//results_fScore << "True Positive: " << TruePositive << "\n";
			//results_fScore << "False Negative: " << FalseNegative << "\n";
			//results_fScore << "False Positive: " << FalsePositive << "\n";

			precision = (double)TruePositive / (double)(TruePositive + FalsePositive);
			recall = (double)TruePositive / (double)(TruePositive + FalseNegative);

			//results_fScore << "precision: " << precision << "\n";
			//results_fScore << "recall: " << recall << "\n\n";

			if (precision != 0 && recall != 0){
				double tempFScore = 2 * (precision * recall) / (precision + recall);
				fScoresPerOne.push_back(tempFScore);
			}
			else
				fScoresPerOne.push_back(0);

		} // end of for loop for real circles
		//results_fScore << "FScore-ovi po jednom predicted: " << "\n";
		//for (int k = 0; k < fScoresPerOne.size(); k++){
		//results_fScore << fScoresPerOne[k] << " ";
		//}

		fMax = fScoresPerOne[0];
		double avegPerOne = 0;
		for (int k = 0; k < fScoresPerOne.size(); k++){
			avegPerOne += fScoresPerOne[k];
			if (fScoresPerOne[k]> fMax)
				fMax = fScoresPerOne[k]; //F1-score - max of Fscore calculated  for one predicted and all actual
		}
		avegPerOne = avegPerOne / fScoresPerOne.size();
		//results_fScore << "Maximum: " << fMax << "\n";
		//results_fScore << "Average: " << avegPerOne << "\n\n";
		fMaxPerOnePredicted.push_back(fMax);

		fScoresPerOne.clear();
	} // end of for loop for predicted circles

	//results_fScore << "Max FScore-ovi po svim predicted: " << "\n";
	//for (int p = 0; p < fMaxPerOnePredicted.size(); p++){
	//results_fScore << fMaxPerOnePredicted[p] << " ";
	//	}
	//results_fScore << "\n";

	std::sort(fMaxPerOnePredicted.begin(), fMaxPerOnePredicted.end());
	std::reverse(fMaxPerOnePredicted.begin(), fMaxPerOnePredicted.end());

	//results_fScore << "Max FScore-ovi po svim predicted SORTIRANI: " << "\n";
	//for (int p = 0 ; p <fMaxPerOnePredicted.size(); p++){
	//results_fScore << fMaxPerOnePredicted[p] << " ";
	//}
	//results_fScore << "\n";

	vector<double> bestOnes;

	int lenghtBestOnes;

	if (realCircles.size() > circles.size()){
		lenghtBestOnes = circles.size();
	}
	else {
		lenghtBestOnes = realCircles.size();
	}
	results_fScore << "lenght of best ones " << lenghtBestOnes << "\n";
	results_fScore << "lenght of predicted ones " << fMaxPerOnePredicted.size() << "\n";
	for (int p = 0; p < lenghtBestOnes; p++){
		bestOnes.push_back(fMaxPerOnePredicted[p]);
	}

	//results_fScore << "Best ones "<< "\n";
	//for (int p = 0; p < lenghtBestOnes; p++){
	//results_fScore << bestOnes[p] << " ";
	//	}
	//results_fScore << "\n ";

	fScoreAvg = 0;
	double fMaxTotal = bestOnes[0];
	double fMinTotal = bestOnes[0];
	for (int k = 0; k < lenghtBestOnes; k++){
		fScoreAvg += bestOnes[k];

		if (bestOnes[k] > fMaxTotal)
			fMaxTotal = bestOnes[k];

		if (bestOnes[k] < fMinTotal)
			fMinTotal = bestOnes[k];
	}

	//fScoreAvg = fScoreAvg / bestOnes.size();
	fScoreAvg = fScoreAvg / lenghtBestOnes;
	results_fScore << fMinTotal << "\n";
	results_fScore << fMaxTotal << "\n";
	results_fScore << fScoreAvg;

	return fScoreAvg;
}



vector<int> make_Wo(vector<vector<int> > similarCircles){
	vector<int> Wo;
	int k;
	Wo.clear();
	for (int i = 0; i < similarCircles.size(); i++){
		k = 0;
		for (int j = 0; j < similarCircles[i].size(); j++){
			k++;
		}
		Wo.push_back(k);
	}
	cout << "Number of elements in each row in real data(circle): " << endl;
	for (int i = 0; i < Wo.size(); i++) {
		cout << Wo[i] << " ";
	}
	cout << endl;

	return Wo;
}

void find_similar_circles_one(vector<vector<int>> &F, vector<vector<int> > &circles, vector<vector<int>> nodes, vector<int> ego, vector<int> users, int maxNumOfIterations, double trashhold, double coeffWeight_1, double coeffWeight_0){

	int k;
	double sum = 0;
	int num_ones, num_zeros;
	vector<vector<int>> filteredCircles;
	vector<vector<int>> filteredNodes;
	vector<int> filteredUsers;

	k = 0;
	vector<int> tempFilter;
	for (int i = 0; i < nodes.size(); i++)
	{
		std::vector<int> temp;
		num_ones = 0;
		num_zeros = 0;
		for (int j = 0; j < nodes[0].size(); j++) {
			if (nodes[i][j] == ego[j])
			{
				if (ego[j] == 1)
					num_ones++;
				else
					num_zeros++;
			}

		}
		double sum = num_ones*coeffWeight_1 + num_zeros*coeffWeight_0;
		cout << "weight " << sum << endl;

		if (sum > trashhold){
			filteredUsers.push_back(users[i]);
			filteredNodes.push_back(nodes[i]);
		}

	}

	cout << "koji useri: " << endl;
	for (int p = 0; p < filteredUsers.size(); p++)
		cout << filteredUsers[p] << " ";
	cout << endl;
	if (filteredNodes.size() != 0){
		for (int j = 0; j < filteredNodes[0].size() && circles.size() < maxNumOfIterations; j++) // iterira kroz svakog korisnika
		{
			vector<int> tempF;
			vector<int> temp;
			for (int i = 0; i < filteredNodes.size(); i++) // filtere 
			{
				if (filteredNodes[i][j] == ego[j]){
					temp.push_back(filteredUsers[i]);
				}
			}

			sort(temp.begin(), temp.end());
			circles.push_back(temp);
			tempF.push_back(j);
			F.push_back(tempF);

		}
	}
}


void find_similar_circles_multiple(vector<vector<int>> &F, vector<vector<int> > &circles, vector<int> &ego, int maxNumOfIterations){
	int l;
	int NoCircles;

	int lnew = 0;
	int lo = 0; // id number of the starting circle
	set <vector<int>> Fset;
	vector<int> C; // vector as an output of a function
	l = lnew = circles.size();
	cout << "Lenght" << l << endl;
	int duplicated = 0;
	if (circles.size() >= maxNumOfIterations) {
		return;
	}

	for (int i = 0; i < F.size(); i++){
		Fset.insert(F[i]);
	}

	long printCounter = 0;

	do {
		NoCircles = 0;
		for (int i = lo; i < (l - 1) && circles.size() < maxNumOfIterations; i++){
			for (int j = i + 1; j < l && circles.size() < maxNumOfIterations; j++){
				if (++printCounter % 100000 == 0){
					cout << "Poredim C" << i << "  sa C" << j << endl;
					printCounter = 0;
				}
				vector<int> C = intersection(circles[i], circles[j]);
				if (C.size() > 0){

					vector<int> tempF;
					tempF.insert(tempF.end(), F[i].begin(), F[i].end());
					tempF.insert(tempF.end(), F[j].begin(), F[j].end());
					tempF = remove_double_features(tempF);

					if (Fset.find(tempF) == Fset.end()){
						F.push_back(tempF);
						Fset.insert(tempF);
						circles.push_back(C);
						lnew++;
						NoCircles++;
					}

				}

			}


		}
		lo = l;
		l = lnew;
		//lets_check(circles);

	} while (NoCircles > 0);

}

double F_Score_similarity(vector<vector<int> > &circles, vector<vector<int> > &realCircles, vector<int> &Mo, vector<int> &Wo){
	double fScoreAvg = 0;
	vector <int> weight_predicted, actual;
	vector <double>    fScoresPerOne;
	int No_True_Positive;
	double recall, precision, fMax;
	int n, m;
	vector<double> fMaxPerOnePredicted;
	ofstream results_fScoreSimilarity("fscore_similarity.txt");

	for (int i = 0; i < circles.size(); i++){

		weight_predicted.clear();
		for (int k = 0; k < circles[i].size(); k++){
			int val = circles[i][k];
			weight_predicted.push_back(val);
		}

		for (int j = 0; j < realCircles.size(); j++){
			actual.clear();
			for (int k = 0; k < realCircles[j].size(); k++){
				int val = realCircles[j][k];
				actual.push_back(val);
			}

			int TruePositive = 0;
			int FalsePositive = 0;
			int FalseNegative = 0;
			bool match = false;

			for (int p = 0; p < weight_predicted.size(); p++){
				match = false;
				for (int k = 0; k < actual.size(); k++){
					if (weight_predicted[p] == actual[k]){
						match = true;
						break;
					}
				}
				if (match){
					TruePositive++;
				}
				else{
					FalsePositive++;
				}
			}

			for (int k = 0; k < actual.size(); k++){
				match = false;
				for (int p = 0; p < weight_predicted.size(); p++){
					if (actual[k] == weight_predicted[p]){
						match = true;
						break;
					}
				}
				if (!match){
					FalseNegative++;
				}
			}

			precision = (double)TruePositive / (double)(TruePositive + FalsePositive);
			recall = (double)TruePositive / (double)(TruePositive + FalseNegative);

			if (precision != 0 && recall != 0){
				double tempFScore = 2 * (precision * recall) / (precision + recall);
				fScoresPerOne.push_back(tempFScore);
			}
			else
				fScoresPerOne.push_back(0);

		}
		fMax = fScoresPerOne[0];
		for (int k = 0; k < fScoresPerOne.size(); k++){
			if (fScoresPerOne[k]> fMax)
				fMax = fScoresPerOne[k]; //F1-score - max of Fscore calculated  for one predicted and all actual
		}
		fMaxPerOnePredicted.push_back(fMax);

		fScoresPerOne.clear();
	}

	std::sort(fMaxPerOnePredicted.begin(), fMaxPerOnePredicted.end());
	std::reverse(fMaxPerOnePredicted.begin(), fMaxPerOnePredicted.end());

	vector<double> bestOnes;

	int lenghtBestOnes;

	if (realCircles.size() > circles.size()){
		lenghtBestOnes = circles.size();
	}
	else {
		lenghtBestOnes = realCircles.size();
	}
	results_fScoreSimilarity << "lenght of best ones " << lenghtBestOnes << "\n";
	results_fScoreSimilarity << "lenght of predicted ones " << fMaxPerOnePredicted.size() << "\n";
	for (int p = 0; p < lenghtBestOnes; p++){
		bestOnes.push_back(fMaxPerOnePredicted[p]);
	}

	fScoreAvg = 0;
	double fMaxTotal = bestOnes[0];
	double fMinTotal = bestOnes[0];
	for (int k = 0; k < lenghtBestOnes; k++){
		fScoreAvg += bestOnes[k];

		if (bestOnes[k] > fMaxTotal)
			fMaxTotal = bestOnes[k];

		if (bestOnes[k] < fMinTotal)
			fMinTotal = bestOnes[k];
	}

	fScoreAvg = fScoreAvg / lenghtBestOnes;
	results_fScoreSimilarity << fMinTotal << "\n";
	results_fScoreSimilarity << fMaxTotal << "\n";
	results_fScoreSimilarity << fScoreAvg;

	return fScoreAvg;
}