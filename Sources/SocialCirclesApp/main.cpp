#include "cluster.hpp"
#include "util.hpp"
#include "string.h"
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

/** Code to run the experiment and save the output in a nice readable format. */
void experiment(graphData* gd,
	int K, // number of circles
	int lambda, // regularization parameter
	int reps, // number of iterations of training
	int gradientReps, // number of iterations of gradient ascent
	int improveReps, // number of iterations using by QPBO
	char* resName // Where to save the results
	)
{
	long starttime = clock();
	printf("Results will be saved to %s.\n", resName);

	Cluster c(gd);
	Scalar bestll = 0;
	int bestseed = 0;
	vector<set<int> > bestClusters;
	Scalar* bestTheta = new Scalar[K*c.gd->nEdgeFeatures];
	Scalar* bestAlpha = new Scalar[K*c.gd->nEdgeFeatures];

	// Number of random restarts
	int nseeds = 5;

	for (int seed = 0; seed < nseeds; seed++)
	{
		printf("Random restart %d of %d\n", seed + 1, nseeds);
		c.train(K, reps, gradientReps, improveReps, lambda, seed, SYMMETRICDIFF);
		Scalar ll = c.loglikelihood(c.theta, c.alpha, c.chat);
		if (ll > bestll or bestll == 0)
		{
			bestll = ll;
			bestseed = seed;
			bestClusters = c.chat;
			memcpy(bestTheta, c.theta, K*c.gd->nEdgeFeatures*sizeof(Scalar));
			memcpy(bestAlpha, c.alpha, K*sizeof(Scalar));
		}
	}

	FILE* f = fopen(resName, "w");

	long endtime = clock();

	fprintf(f, "\{\n");
	fprintf(f, "\t\"seed\" : %d,\n", bestseed);
	fprintf(f, "\t\"ll\" : %f,\n", bestll);
	fprintf(f, "\t\"loss_zeroone\" : %f,\n", totalLoss(c.gd->clusters, bestClusters, c.gd->nNodes, ZEROONE));
	fprintf(f, "\t\"loss_symmetric\" : %f,\n", totalLoss(c.gd->clusters, bestClusters, c.gd->nNodes, SYMMETRICDIFF));
	fprintf(f, "\t\"fscore\" : %f,\n", 1 - totalLoss(c.gd->clusters, bestClusters, c.gd->nNodes, FSCORE));
	fprintf(f, "\t\"clusters\" : [");
	for (vector<set<int> >::iterator it = bestClusters.begin(); it != bestClusters.end(); it++)
	{
		if (it != bestClusters.begin()) fprintf(f, ",");
		fprintf(f, "[");
		for (set<int>::iterator it2 = it->begin(); it2 != it->end(); it2++)
		{
			if (it2 != it->begin()) fprintf(f, ",");
			fprintf(f, "%s", gd->indexNode[*it2].c_str());
		}
		fprintf(f, "]");
	}
	fprintf(f, "],\n");

	fprintf(f, "\t\"theta\" : [");
	for (int k = 0; k < K; k++)
	{
		if (k != 0) fprintf(f, ",");
		fprintf(f, "[");
		for (int i = 0; i < c.gd->nEdgeFeatures; i++)
		{
			if (i != 0) fprintf(f, ",");
			fprintf(f, "%f", bestTheta[k*c.gd->nEdgeFeatures + i]);
		}
		fprintf(f, "]");
	}
	fprintf(f, "],\n");

	fprintf(f, "\t\"alpha\" : [");
	for (int k = 0; k < K; k++)
	{
		if (k != 0) fprintf(f, ",");
		fprintf(f, "%f", bestAlpha[k]);
	}
	fprintf(f, "],\n");

	fprintf(f, "\t\"runtime\" : %f\n", ((float)(endtime - starttime)) / CLOCKS_PER_SEC);
	fprintf(f, "\}\n");
	fclose(f);

	delete[] bestTheta;
	delete[] bestAlpha;
}

int main(int argc, char** argv)
{
	int c;
	opterr = 0;

	// experiment parameters
	int k = 3;
	int lambda = 1;
	int trainingIterations = 25;
	int gradientReps = 50;
	int QPBOreps = 5;
	char *id = NULL;
	char *resultFile = NULL;

	while ((c = getopt(argc, argv, ":i:k:l:t:g:q:r:")) != -1)
	{		
		switch (c)
		{
		case 'i':
			if (optarg != NULL)
				id = optarg;
			break;
		case 'k':
			if (optarg != NULL)
				k = atoi(optarg);
			break;
		case 'l':
			if (optarg != NULL)
				lambda = atoi(optarg);
			break;
		case 't':
			if (optarg != NULL)
				trainingIterations = atoi(optarg);
			break;
		case 'g':
			if (optarg != NULL)
				gradientReps = atoi(optarg);
			break;
		case 'q':
			if (optarg != NULL)
				QPBOreps = atoi(optarg);
			break;
		case 'r':
			if (optarg != NULL)
				resultFile = optarg;
			break;
		default:
			return 0;
		}
	}
	
	if (argc < 2)
	{
		printf("Expected 2 arguments (userid, output path), e.g.\n");
		printf("%s facebook/698 results.out\n", argv[0]);
		exit(1);
	}

	printf("Running with configuration: K: %d, Lambda: %d, Training operations: %d, Gradient reps: %d, QPBO reps: %d.\n", k, lambda, trainingIterations, gradientReps, QPBOreps);

	char* nodeFeatureFile = new char[1000];
	char* selfFeatureFile = new char[1000];
	char* clusterFile = new char[1000];
	char* edgeFile = new char[1000];

	sprintf(nodeFeatureFile, "%s.feat", id);
	sprintf(selfFeatureFile, "%s.egofeat", id);
	sprintf(clusterFile, "%s.circles", id);
	sprintf(edgeFile, "%s.edges", id);

	graphData gd(nodeFeatureFile, selfFeatureFile, clusterFile, edgeFile, FRIENDFEATURES, 0);
	experiment(&gd,
		k, // K
		lambda, // lambda
		trainingIterations, // training iterations
		gradientReps, // gradient reps
		QPBOreps, // QPBO reps
		resultFile);

	delete[] nodeFeatureFile;
	delete[] selfFeatureFile;
	delete[] clusterFile;
	delete[] edgeFile;
	
	exit(EXIT_SUCCESS);
}
