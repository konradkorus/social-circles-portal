################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../qpbo/QPBO.cpp \
../qpbo/QPBO_extra.cpp \
../qpbo/QPBO_maxflow.cpp \
../qpbo/QPBO_postprocessing.cpp 

OBJS += \
./qpbo/QPBO.o \
./qpbo/QPBO_extra.o \
./qpbo/QPBO_maxflow.o \
./qpbo/QPBO_postprocessing.o 

CPP_DEPS += \
./qpbo/QPBO.d \
./qpbo/QPBO_extra.d \
./qpbo/QPBO_maxflow.d \
./qpbo/QPBO_postprocessing.d 


# Each subdirectory must supply rules for building sources it contributes
qpbo/%.o: ../qpbo/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


